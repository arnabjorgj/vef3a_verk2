<?php
/**
 * Created by PhpStorm.
 * User: arnajonasar
 * Date: 07/02/16
 * Time: 21:04



1) Af hverju ættir þú að nota OOP í PHP? Hvenær væri það hugsanlega heppilegt og
hvenær ekki. Hver er munurinn á procedural programming og OOP. Komdu með dæmi
og rökstuðning.
*/
/*
OOP er mjög heppilegt þegar nota á sama kóða oftar en einu sinni. Með OOP er hægt að búa til
object og nota þannig sama kóðann aftur með því að kalla í objectið. Þetta gerir manni kleift
að gera flóknari hluti með minni kóða.
	Með procedural programming les þýðandinn kóðann frá toppi og niður og fer aldrei til
baka, á meðan með OOP er hægt að búa til object hvar sem er í kóðanum og notað það hvar sem
er með því að kalla í það.
*/
/**
2) Hver er munurinn á public og private members þegar kemur að: a) property b) methods
*/
/*
 * a) Þegar breyta er public property er hún aðgengileg hvar sem er, bæði inni í klasanum og fyrir
 * utan hann. Private breyta sem er inni í klasa er ekki aðgengileg fyrir utan hann nema hún sé
 * í falli, þá er hægt að kalla í fallið breytan verður aðgengileg. Þegar það er búinn til klasi
 * innan klasans (child class) er public breytan aðgengileg innan hans en ekki private.
 * b) Virkar eins og með property nema það er ekki hægt að nálgast private breytuna í gegnum
 * function.
 */

/**
 * 3) Hvað er namespaces og Autoloading?
 */
/*
 * namespace er hentugt þegar verið er að nota kóða innan kóða (include). Það gerist stundum að
 * breytur eða function séu með sömu nöfn í mismunandi kóða sem er include-aður. Hægt er að komast
 * hjá því að breyta nöfnum með því að setja namespace utan um kóðan.
 * Autoloading er hentugt þegar maður ætlar að includea margar skrár í sama kóðann. Dæmi:
 */

// Hér er verið að nota MyClass1 og MyClass2 úr skránum MyClass1.php og MyClass2.php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$obj  = new MyClass1();
$obj2 = new MyClass2();

/**
 * 4) Útskýrðu Interfaces
 */
/*
 * Þegar klasi er búinn til bara til þess að vera parent class yfir öðrum klösum, er líklega betra
 * að vera með interface. Interface leyfir þér að búa til "common structure" fyrir klasana þína.
 * Til dæmis hentugt þegar fleiri en einn er að vinna í sama kóða, þá getur einn búið til
 * interface fyrir hina til að fylgja, en þeir sjá um smáatriðin. Það er ekki hægt að búa til
 * instance af interface klasa. Í staðinn er búinn til klasi sem implementar interface, og þannig
 * er hægt að nálgast það sem er í interfaceinu.
 */

/**
 * 5) Búðu til með php class sem heitir Bók sem hefur eftirfarandi:
 * properties; titill, verð.
 * Methods; setPrice, getPrice, setTitle, getTitle.
 * Smið sem tekur tvo parametra (titil og verð)
 */

class Bok {
    public $title = "";
    public $price = 0;

    public function setPrice($price) {
        $this->price = $price;
    }
    public function getPrice() {
        return $this->price;
    }
    public function setTitle($title) {
        $this->title = $title;
    }
    public function getTitle() {
        return $this->title;
    }

    function __construct($title, $price) {

        $this->title = $title;
        $this->price = $price;
    }
}

/**
 * 6) Notaði Bók class og gerðu eftirfarandi:
 * búðu til þrjú object; $efnafraedi, $staerdfraedi og $islenska
 * Gefðu þessum objectum titil og verð.
 * */

$efnafraedi = new Bok("Efnafræði",5000);
$staerdfraedi = new Bok("Stæ 503", 6000);
$islenska = new Bok("Íslenska tvö", 2000);

/**
 * 7) Búðu til class sem erfir (extends) Bók class. Bættu við eigindinu publisher og
 * tveimur aðferðum getPublisher og setPublisher. Birtu Bókaupplýsingarnar.
 */

class Publish extends Bok {
    // Hvað er eigindi?

    public $publisher = "";

    public function getPublisher() {
        return $this->publisher;
    }
    public function setPublisher($publisher) {
        $this->publisher = $publisher;
    }
}

/**
 * 8) Búðu til class með OOP aðferðinni sem heitir User sem geymir gögn um notanda.
 * User þarf að hafa eftirfarandi:
 * Properties; email og password.
 * Methods; setPassword(), getPassword(), setEmail(), getEmail()
 * Smiður sem tekur array sem parameter og upphafsstillir notanda.
 */

Class User {
    public $email = "";
    public $password = "";

    public function setPassword($password) {
        $this->password = $password;
    }
    public function getPassword() {
        return $this->password;
    }
    public function setEmail($email) {
        $this->email = $email;
    }
    public function getEmail() {
        return $this->email;
    }

    function __construct($array)
    {
        $this->email;
        $this->password;
    }
}


